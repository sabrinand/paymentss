<?php

use Illuminate\Database\Seeder;

class CardsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cards')->insert([
            'numero_car' => '4485298887381057',
            'nome_titular' => 'Caio S Rodrigues',
            'cvv' => '129',
            'validade' => '10/29',
            'tipo_status'=> '0',
        ]);

        DB::table('cards')->insert([
            'numero_car' => '4024007114907461',
            'nome_titular' => 'Raissa R Correia',
            'cvv' => '368',
            'validade' => '03/20',
            'tipo_status'=> '1',
        ]);

        DB::table('cards')->insert([
            'numero_car' => '4929939068247666',
            'nome_titular' => 'Giovanna Carvalho',
            'cvv' => '120',
            'validade' => '05/29',
            'tipo_status'=> '2',
        ]);

    }
}
