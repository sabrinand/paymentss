<?php

namespace  App\Repositories;

use App\Payment;

class PaymentRepository
{
  protected $modelClass = Payment::class;

  /**
     * Create a new payment instance after a valid registration.
     *
     * @param  int  $card_number
     * @param  float  $amount
     * @param  string  $requester
     * @return \App\Payment
     */
    public function create(int $card_number, string $amount, string $requester)
    {
        $payment = new Payment;
        $payment->numero_car = $card_number;
        $payment->valor = $amount;
        $payment->entidade = $requester;
        $payment->save();

        return $payment;
    }
}







