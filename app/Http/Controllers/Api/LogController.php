<?php

namespace App\Http\Controllers\Api;

use App\Log;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LogController extends Controller
{
    private $origem;

    public function __construct(Request $request)
    {
        $this->origem = $request->url();
    }

    /**
     * Recovers all registered logs
     *
     * @return json
     */
    public function get(Request $request)
    {
        //Registra a chamada à API
        $this->store($this->origem, 'logs');

        $logs = Log::all();

        $response = [
            'response' => true,
            'logs' => $logs
        ];

        return response()->json($response);
    }

    /**
     * Register a new log
     *
     * @return json
     */
    public function store(string $origin, string $route)
    {
        $log = new Log;
        $log->origem = $origin;
        $log->route = $route;
        $log->save();
    }

}
