<?php

namespace App\Http\Controllers\Api;

use App\Card;
use App\Payment;
use App\Repositories\PaymentRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Api\LogController;
use App\Http\Requests\PaymentChargeRequest;

class PaymentController extends Controller
{
    private $log, $origem;

    public function __construct(Request $request)
    {
        $this->origem = $request->url();
        $this->log = new LogController($request);
    }

    /**
     * Payment logic
     *
     * @return json
     */
    public function charge(PaymentChargeRequest $request,  PaymentRepository $paymentRepository)
    {
        //Registra a chamada à API
        $this->log->store($this->origem, 'charge');

        $card = Card::where('numero_car', $request->card_number)->first();

        //Valida as informações do cartão
        if ($request->card_holder != $card['nome_titular']) {
            $response = [
                'response' => false,
                'type' => 'error',
                'message' => 'Nome do titular incorreto.',
                'text' => ''
            ];
        } elseif ($request->expiration_date != $card['validade']) {
            $response = [
                'response' => false,
                'type' => 'error',
                'message' => 'Validade incorreta.',
                'text' => ''
            ];
        } elseif ($request->card_cvc != $card['cvv']) {
            $response = [
                'response' => false,
                'type' => 'error',
                'message' => 'CVV incorreto.',
                'text' => ''
            ];
        } else {
            /*
            * Verifica o status do cartão
            * 0 - Aprova | 1 - Reprova | 2 - Random
            */
            switch ($card['tipo_status']) {
                case 0:
                    $response = [
                        'response' => true,
                        'type' => 'success',
                        'message' => 'Compra aprovada!',
                        'text' => 'Obrigada por adquirir nossos produtos! Os detalhes da compra serão enviados por e-mail.'
                    ];
                    break;

                case 1:
                    $response = [
                        'response' => false,
                        'type' => 'error',
                        'message' => 'Compra reprovada.',
                        'text' => 'Revise os dados do seu cartão de crédito e tente novamente.'
                    ];
                    break;

                case 2:
                    $status = array('Compra aprovada', 'Compra reprovada.');
                    $type = array('success', 'error');
                    $message = array(true, false);
                    $text = array('Obrigada por adquirir nossos produtos! Os detalhes da compra serão enviados por e-mail.', 'Revise os dados do seu cartão de crédito e tente novamente.');
                    $random = rand(0, 1);

                    $response = [
                        'response' => $status[$random],
                        'type' => $type[$random],
                        'message' => $message[$random],
                        'text' => $text[$random]
                    ];
                    break;
            }

            //Registra a compra, caso tenha sido aprovada
            if($response['response']) {
                $paymentRepository->create($request->card_number,
                $request->amount,
                $request->requester);
            }
        }

        return response()->json($response);
    }

    /**
     * Recovers all registered payments
     *
     * @return json
     */
    public function getPayments()
    {
        //Registra a chamada à API
        $this->log->store($this->origem, 'payments');

        $payments = Payment::all();

        $response = [
            'response' => true,
            'payments' => $payments
        ];

        return response()->json($response);
    }

    /**
     * Recover a specific payment
     *
     * @return json
     */
    public function getPayment(int $id)
    {
        //Registra a chamada à API
        $this->log->store($this->origem, 'payment');

        $payment = Payment::where('id', $id)->get();

        $response = [
            'response' => true,
            'payment' => $payment
        ];

        return response()->json($response);
    }
}
