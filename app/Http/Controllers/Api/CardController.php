<?php

namespace App\Http\Controllers\Api;

use App\Card;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Api\LogController;

class CardController extends Controller
{
    private $log, $origem;

    public function __construct(Request $request)
    {
        $this->origem = $request->url();
        $this->log = new LogController($request);
    }

    /**
     * Recovers all registered cards
     *
     * @return json
     */
    public function get(Request $request)
    {
        //Registra a chamada à API
        $this->log->store($this->origem, 'cards');

        $cards = Card::all();

        $response = [
            'response' => true,
            'cards' => $cards
        ];

        return response()->json($response);
    }
}
