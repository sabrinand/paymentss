<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PaymentChargeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'card_holder' => 'required',
            'expiration_date' => 'required',
            'card_number' => 'required|numeric',
            'card_cvc' => 'required|numeric',
            'amount' => 'required',
            'requester' => 'required',
        ];
    }
}
