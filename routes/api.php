<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/charge', 'Api\PaymentController@charge');
Route::get('/payments', 'Api\PaymentController@getPayments');
Route::get('/payment/{id}', 'Api\PaymentController@getPayment');

Route::get('/logs', 'Api\LogController@get');

Route::get('/cards', 'Api\CardController@get');

Route::post('/test', function () {
    return response('Test API', 200)
                  ->header('Content-Type', 'application/json');
});
