<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PaymentsTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testCreatePayment()
    {
        \App\Payment::create([
            'numero_car' => "4929939068247666",
            'valor' => "350.00",
            'entidade' => 'Teste',
            
        ]);

        $this->assertDatabaseHas('payments', ['entidade'=>'Teste']);
    }
}
